/**
 * Copyright &copy; 2017-2019 <a href="https://gitee.com/baseweb/JSite">JSite</a> All rights reserved.
 */
package com.jsite.modules.flowable.dao;

import com.jsite.common.persistence.CrudDao;
import com.jsite.common.persistence.annotation.MyBatisDao;
import com.jsite.modules.flowable.entity.FormData;

import java.util.List;

/**
 * 表单数据表生成DAO接口
 * @author liuruijun
 * @version 2019-04-01
 */
@MyBatisDao
public interface FormDataDao extends CrudDao<FormData> {
    List<FormData> findTempList(FormData formData);

    FormData getTemp(FormData formData);

    int insertTemp(FormData formData);

    int updateTemp(FormData formData);

    int deleteTemp(FormData formData);
}