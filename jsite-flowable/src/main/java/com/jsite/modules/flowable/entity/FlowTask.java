package com.jsite.modules.flowable.entity;

import com.google.common.collect.Lists;
import com.jsite.modules.file.entity.AppFile;
import com.jsite.modules.sys.entity.User;

import java.util.List;

public class FlowTask extends Flow {

    private static final long serialVersionUID = 1L;

    public FlowTask() {
        super();
    }

    private User startUser;

    private String title;

    private List<AppFile> fileList = Lists.newArrayList();

    public User getStartUser() {
        return startUser;
    }

    public void setStartUser(User startUser) {
        this.startUser = startUser;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<AppFile> getFileList() {
        return fileList;
    }

    public void setFileList(List<AppFile> fileList) {
        this.fileList = fileList;
    }

}
