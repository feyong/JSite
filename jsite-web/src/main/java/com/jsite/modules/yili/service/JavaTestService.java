/**
 * Copyright &copy; 2017-2019 <a href="https://gitee.com/baseweb/JSite">JSite</a> All rights reserved.
 */
package com.jsite.modules.yili.service;

import com.jsite.common.persistence.Page;
import com.jsite.common.service.CrudService;
import com.jsite.modules.yili.dao.JavaTestDao;
import com.jsite.modules.yili.entity.JavaTest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * JAVA机试表生成Service
 * @author liuruijun
 * @version 2020-06-09
 */
@Service
@Transactional(readOnly = true)
public class JavaTestService extends CrudService<JavaTestDao, JavaTest> {

	@Override
	public JavaTest get(String id) {
		return super.get(id);
	}

	public JavaTest getByName(String name, String officeID) {
		return dao.getByName(name, officeID);
	}

	public int getCount() {
		return dao.getCount();
	}

	@Override
	public List<JavaTest> findList(JavaTest javaTest) {
		return super.findList(javaTest);
	}
	
	@Override
	public Page<JavaTest> findPage(Page<JavaTest> page, JavaTest javaTest) {
		return super.findPage(page, javaTest);
	}
	
	@Override
	@Transactional(readOnly = false)
	public void save(JavaTest javaTest) {
		super.save(javaTest);
	}
	
	@Override
	@Transactional(readOnly = false)
	public void delete(JavaTest javaTest) {
		super.delete(javaTest);
	}
	
}